# ecrit par Xue
@Register
Feature: Register

  @Test_PE-39
  Scenario: Register by filling in all mandatory fields
    Given I am on the homepage
		When I click button My Account
		Then The submenu of My Account appears
		When I click link Register
		Then The page Register appears
		When I fill in text fields 'FirstName' 'LastName' 'test@test.com' 'Telephone' 'Password' 'Password'
		And I check the check box Privacy Policy and I click button Continue
		Then The page Account Created appears

  @Test_PE-40
  Scenario: Register with the option to subscribe newsletters
    Given I am on the homepage
		When I click button My Account
		Then The submenu of My Account appears
		When I click link Register
		Then The page Register appears
		When I fill in text fields 'FirstName' 'LastName' 'test@test.com' 'Telephone' 'Password' 'Password'
		And I check the radio button Yes of Subscribe Newsletter
		And I check the check box Privacy Policy and I click button Continue
		Then The page Account Created appears
	
	
	@Test_PE-41
	Scenario: User cannot create account with the same email address
		Given Create an account 'FirstName' 'LastName' 'pre1@condition.com' 'Telephone' 'Password' 'Password'
		And I am on the homepage
		When I click button My Account
		Then The submenu of My Account appears
		When I click link Register
		Then The page Register appears
		When I fill in text fields 'FirstName' 'LastName' 'pre1@condition.com' 'Telephone' 'Password' 'Password'
		And I check the check box Privacy Policy and I click button Continue
		Then I valid that Warning message 'Warning: E-Mail Address is already registered!' on the page
		
	@Test_PE-42
	Scenario: Log in with vaild data
		Given Create an account 'FirstName' 'LastName' 'pre2@condition.com' 'Telephone' 'Password' 'Password'
		And I am on the homepage	
		When I click button My Account
		Then The submenu of My Account appears
		When I click link Login
		Then Page Login appears
		When I enter Email Address 'pre2@condition.com' and Password 'Password'
		And I click button Login
		Then Page Account appears
	
	@Test_PE-43
	Scenario: Reinitiate password
		Given Create an account 'FirstName' 'LastName' 'pre3@condition.com' 'Telephone' 'Password' 'Password'
		And I am on the homepage
		When I click button My Account
		Then The submenu of My Account appears
		When I click link Login
		Then Page Login appears
		When I click link Forgotten Password
		Then Page ForgottenPassword appears
		When I enter Email Address 'pre3@condition.com' and I click button Continue
		Then I valide confirmation message 'An email with a confirmation link has been sent your email address.'
		
	@Test_PE-44
	Scenario: Search products with valid data
		Given I am on the homepage
		When I enter 'iphone' in the search textfield and I click button Search
		Then I valid search results on the page
		
	@Test_PE-45
	Scenario: Search products with invalid data
		Given I am on the homepage
		When I enter 'italk' in the search textfield and I click button Search
		Then I valid messge 'There is no product that matches the search criteria.' on the page
		
	@Test_PE-46
	Scenario: Place an order
		Given Create an account 'FirstName' 'LastName' 'pre4@condition.com' 'Telephone' 'Password' 'Password'
		And I am on the homepage
		When I enter 'MacBook' in the search textfield and I click button Search
		Then I valid search results on the page
		When I click button ADD TO CART of 'MacBook'
		And I click link Checkout
		Then The page Checkout appears
		When I enter Email Address 'pre4@condition.com' and Password 'Password' and I click button Login
		Then Step Billing Details appears
		When I entre FirstName 'David' LastName 'Deluc' Address1 '123 ABC' City 'Tokyo' Postcode 'H1H1H1'
		And I click dropdown menu of Country and I select Coutry 'Japan'
		And I click dropdown menu of Region/State and I select 'Tokyo'
		And I click button Continue of Step Billing Details
		Then Step Payment Method appears
		When I check the checkbox TermsCondition and I click button Continue
		Then I valid that Warning message 'Warning: Payment method required!' is not on the page

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		