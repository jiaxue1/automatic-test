package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"features"},
		glue = {"steps"},
		monochrome = true,
		dryRun = true,
		plugin= {"pretty","html:EcommerceReport"}
		
		)

public class TestRunner {

}
