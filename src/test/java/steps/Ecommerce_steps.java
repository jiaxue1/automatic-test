package steps;

import static org.junit.Assert.fail;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Ecommerce_steps {
	public WebDriver driver;
	
	DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    Date date = new Date();
    String date_current = dateFormat.format(date);
	
	@Given("I am on the homepage")
	public void i_am_on_the_homepage() {
		WebDriverManager.chromedriver();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
	}
	@When("I click button My Account")
	public void i_click_button_my_account() {
	    driver.findElement(By.xpath("//span[text()='My Account']")).click();
	}
	@Then("The submenu of My Account appears")
	public void the_submenu_of_my_account_appears() {
	    if (!driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown-menu-right']")).isDisplayed()) {
	    	System.out.println("The dropdown menu is not displayed");
	    }
	}
	@When("I click link Register")
	public void i_click_link_register() {
	    driver.findElement(By.xpath("//a[text()='Register']")).click();
	}
	@Then("The page Register appears")
	public void the_page_register_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=account/register";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
	}
	@When("I fill in text fields {string} {string} {string} {string} {string} {string}")
	public void i_fill_in_text_fields(String firstname, String lastname, String email, String telephone, String password, String passwordconfirm) {
		
		driver.findElement(By.id("input-firstname")).sendKeys(firstname);
	    driver.findElement(By.id("input-lastname")).sendKeys(lastname);
	    driver.findElement(By.id("input-email")).sendKeys(date_current+email);
	    driver.findElement(By.id("input-telephone")).sendKeys(telephone);
	    driver.findElement(By.id("input-password")).sendKeys(password);
	    driver.findElement(By.id("input-confirm")).sendKeys(passwordconfirm);  
	}
	@When("I check the check box Privacy Policy and I click button Continue")
	public void i_check_the_check_box_privacy_policy_and_i_click_button_continue() {
	    driver.findElement(By.xpath("//input[@name='agree']")).click();
	    driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
	}
	@Then("The page Account Created appears")
	public void the_page_account_created_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=account/success";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
		driver.quit();
	}

	@When("I check the radio button Yes of Subscribe Newsletter")
	public void i_check_the_radio_button_yes_of_subscribe_newsletter() {
	    driver.findElement(By.xpath("//input[@name='newsletter' and @value='1']")).click();
	}


	@Given("Create an account {string} {string} {string} {string} {string} {string}")
	public void create_an_account(String firstname, String lastname, String email, String telephone, String password, String passwordconfirm) throws InterruptedException {
		WebDriver driver;
	    WebDriverManager.chromedriver();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Register']")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("input-firstname")).sendKeys(firstname);
	    driver.findElement(By.id("input-lastname")).sendKeys(lastname);
	    driver.findElement(By.id("input-email")).sendKeys(date_current+email);
	    driver.findElement(By.id("input-telephone")).sendKeys(telephone);
	    driver.findElement(By.id("input-password")).sendKeys(password);
	    driver.findElement(By.id("input-confirm")).sendKeys(passwordconfirm);
	    driver.findElement(By.xpath("//input[@name='agree']")).click();
	    driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
	    Thread.sleep(5000);
	    driver.close();
	}

	@Then("I valid that Warning message {string} on the page")
	public void i_valid_that_warning_message_on_the_page(String expectedMessage) {
		String actualMessage = driver.findElement(By.xpath("//div[@class='alert alert-danger alert-dismissible']")).getText();
	    if (!expectedMessage.equals(actualMessage)) {
			fail("There is no Warning message on the page");
		}
	}


	@When("I click link Login")
	public void i_click_link_login() {
	    driver.findElement(By.xpath("//a[text()='Login']")).click();
	}
	@Then("Page Login appears")
	public void page_login_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=account/login";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
	}
	@When("I enter Email Address {string} and Password {string}")
	public void i_enter_email_address_and_password(String email, String password) {
	    driver.findElement(By.id("input-email")).sendKeys(date_current+email);
	    driver.findElement(By.id("input-password")).sendKeys(password);	    
	}
	@When("I click button Login")
	public void i_click_button_login() {
	    driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
	}
	@Then("Page Account appears")
	public void page_account_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=account/account";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
	}

	@When("I click link Forgotten Password")
	public void i_click_link_forgotten_password() {
	    driver.findElement(By.xpath("//input[@id='input-password']//following-sibling::a")).click();
	}
	@Then("Page ForgottenPassword appears")
	public void page_forgotten_password_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=account/forgotten";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
	}
	@When("I enter Email Address {string} and I click button Continue")
	public void i_enter_email_address_and_i_click_button_continue(String email) {
	    driver.findElement(By.id("input-email")).sendKeys(date_current+email);
	    driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
	}
	@Then("I valide confirmation message {string}")
	public void i_valide_confirmation_message(String expectedMessage) {
		String actualMessage = driver.findElement(By.xpath("//div[@class='alert alert-success alert-dismissible']")).getText();
	    if (!expectedMessage.equals(actualMessage)) {
			fail("There is no Confirmation message on the page");
		}
	}

	@When("I enter {string} in the search textfield and I click button Search")
	public void i_enter_in_the_search_textfield_and_i_click_button_search(String text) {
	    driver.findElement(By.xpath("//input[@name='search']")).sendKeys(text);
	    driver.findElement(By.xpath("//button[@class='btn btn-default btn-lg']")).click();
	}
	@Then("I valid search results on the page")
	public void i_valid_search_results_on_the_page() {
	    if (driver.findElements(By.xpath("//span[text()='Add to Cart']")).size() <= 0) {
	    	fail("The page does not show search results");
	    }
	}

	@Then("I valid messge {string} on the page")
	public void i_valid_messge_on_the_page(String expectedMessage) {
		String actualMessage = driver.findElement(By.xpath("//h2//following-sibling::p")).getText();
	    if (!expectedMessage.equals(actualMessage)) {
			fail("There is no Warning message on the page");
		}
	}


	@When("I click button ADD TO CART of {string}")
	public void i_click_button_add_to_cart_of(String productName) {
	    driver.findElement(By.xpath("//a[text()='"+productName+"']//ancestor::div[@class='caption']//following-sibling::div//span")).click();
	}
	@When("I click link Checkout")
	public void i_click_link_checkout() {
	    driver.findElement(By.xpath("//span[text()='Checkout']")).click();
	}
	@Then("The page Checkout appears")
	public void the_page_checkout_appears() {
		String expectedUrl = "http://tutorialsninja.com/demo/index.php?route=checkout/checkout";
		String actualUrl = driver.getCurrentUrl();
		if (!expectedUrl.equals(actualUrl)) {
			fail("The page is not redirected to the right direction");
		}
	}
	@When("I enter Email Address {string} and Password {string} and I click button Login")
	public void i_enter_email_address_and_password_and_i_click_button_login(String email, String password) throws InterruptedException {
	    Thread.sleep(2000);
		driver.findElement(By.id("input-email")).sendKeys(date_current+email);
	    driver.findElement(By.id("input-password")).sendKeys(password);
	    driver.findElement(By.xpath("//input[@id='button-login']")).click();
	}
	@Then("Step Billing Details appears")
	public void step_billing_details_appears() throws InterruptedException {
		Thread.sleep(2000);
		if(!driver.findElement(By.xpath("//div[@id='payment-new']")).isDisplayed()) {
	    	fail("The page does not show Step Billing details");
	    }
	}
	@When("I entre FirstName {string} LastName {string} Address1 {string} City {string} Postcode {string}")
	public void i_entre_first_name_last_name_address1_city_postcode(String firstname, String lastname, String address, String city, String postcode) {
		driver.findElement(By.id("input-payment-firstname")).sendKeys(firstname);
	    driver.findElement(By.id("input-payment-lastname")).sendKeys(lastname);
	    driver.findElement(By.id("input-payment-address-1")).sendKeys(address);
	    driver.findElement(By.id("input-payment-city")).sendKeys(city);
	    driver.findElement(By.id("input-payment-postcode")).sendKeys(postcode);
	}
	@When("I click dropdown menu of Country and I select Coutry {string}")
	public void i_click_dropdown_menu_of_country_and_i_select_coutry(String coutry) {
	    driver.findElement(By.id("input-payment-country")).click();
	    driver.findElement(By.xpath("//select//option[text()='"+coutry+"']")).click();
	}
	@When("I click dropdown menu of Region\\/State and I select {string}")
	public void i_click_dropdown_menu_of_region_state_and_i_select(String city) throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(By.id("input-payment-zone")).click();
		Thread.sleep(2000);
	    driver.findElement(By.xpath("//select//option[text()='"+city+"']")).click();
	}
	@When("I click button Continue of Step Billing Details")
	public void i_click_button_continue_of_step_billing_details() {
	    driver.findElement(By.id("button-payment-address")).click();
	}
	@Then("Step Payment Method appears")
	public void step_payment_method_appears() throws InterruptedException {
		Thread.sleep(2000);
		if(!driver.findElement(By.xpath("//div[@id='collapse-payment-method']")).isDisplayed()) {
	    	fail("The page does not show Step Payment Method");
	    }
	}
	@When("I check the checkbox TermsCondition and I click button Continue")
	public void i_check_the_checkbox_terms_condition_and_i_click_button_continue() {
	    driver.findElement(By.xpath("//input[@name='agree']")).click();
	    driver.findElement(By.id("button-payment-method")).click();
	}
	
	@Then("I valid that Warning message {string} is not on the page")
	public void i_valid_that_warning_message_is_not_on_the_page(String message) throws InterruptedException {
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//div[@class='alert alert-danger alert-dismissible']")).isDisplayed()){
	    	fail("The page does not show Step Confirm order");
	    }
	}








	



}
